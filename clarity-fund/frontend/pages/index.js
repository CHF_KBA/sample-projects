import React, { Component } from 'react';
import { Card, Button, Header, Dimmer, Loader } from 'semantic-ui-react';
import { Layout } from '../components/Layout'
import { Link } from "../routes";
import { activeCampaign } from './api'


class CampaignIndex extends Component {
    //by putting static no need to create an Instance
    static async getInitialProps(props) {
        const campaigns = await activeCampaign();
        return { campaigns };
    }
    renderCampaigns() {
        if (!this.props.campaigns.length) {
            return <Header as='h1' textAlign='center'>NO ACTIVE CAMPAIGNS!</Header>
        }
        const items = this.props.campaigns.map(campaignData => {
            return (
                <Card key={campaignData.Key}>
                    <Card.Content>
                        <Card.Header>{campaignData.Key}</Card.Header>
                        <Card.Meta><strong>{campaignData.Record.requirement}</strong></Card.Meta>
                        <Card.Description>
                            <div color='red'>Required:{campaignData.Record.amountRequired}₹</div>
                            <div><strong> Collected:{campaignData.Record.amountCollected}₹</strong></div>
                        </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                        <div className='ui buttons'>
                            <Link route={`/campaigns/${campaignData.Key}`}>
                                <Button basic color='green'>
                                    Contribute
                                 </Button>
                            </Link>
                        </div>
                    </Card.Content>
                </Card>
            )
        });
        return (<Card.Group>
            {items}
        </Card.Group>
        );
    }
    render() {
            return (
                <Layout>
                    <div>
                        <Header as="h3">Active Campaigns</Header>
                        <Link route="/campaigns/new">
                            <a>
                                <Button floated="right" content="Create Campaign"
                                    icon="copy"
                                    primary
                                />
                            </a>
                        </Link>

                        {this.renderCampaigns()}
                    </div>
                </Layout>
            );
    }
}

export default CampaignIndex;