import axios from 'axios'

export const activeCampaign = async () => {
    try {
        const payload = { "role": "validator", "id": "validatorAdmin", "args": ["getAllCampaigns", "active"] }
        const result = await axios.post("http://127.0.0.1:3030/newTransaction", payload)
        if (result) {
            return result.data;
        }
    } catch (error) {
        throw new Error(error.response.data)
    }
}

export const allCampaigns = async () => {
    try {
        const payload = { "role": "users", "id": "UsersAdmin", "args": ["getAllCampaigns", "all"] }
        const result = await axios.post("http://127.0.0.1:3030/newTransaction", payload)
        if (result) {
            return result.data;
        }
    } catch (error) {
        throw new Error(error.response.data)
    }
}

export const createCampaign = async ( username, address, contact, reason, amount) => {
    try {
        const payload = { "role": "users", "id": "UsersAdmin", "args": [ username, address, contact, reason, amount] }
        const result = await axios.post("http://127.0.0.1:3030/createCampaign", payload)
        if (result) {
            return true
        }
    } catch (error) {
        throw new Error(error.response.data)
    }
}

export const getCampaign = async (campaignId) => {
    try {
        const payload = { "role": "users", "id": "UsersAdmin", "args": ["readFundingCampaign",campaignId] };
        const result = await axios.post("http://127.0.0.1:3030/newTransaction", payload)
        if (result){
            return result.data;
        }
    } catch (error) {
        throw new Error(error.response.data)
    }
}

export const getRequests = async (campaignId) => {
    try {
        const payload = { "role": "users", "id": "UsersAdmin", "args": ["getRequests",campaignId] }
        const result = await axios.post("http://127.0.0.1:3030/newTransaction", payload)
        if (result){
            return result.data;
        }
    } catch (error) {
        throw new Error(error.response.data)
    }
}

export const approve = async (reqId,username) => {
    try {
        const payload = {"role":"users","id":"UsersAdmin","args":["approveRequest",reqId,username] };
        const result = await axios.post("http://127.0.0.1:3030/newTransaction", payload)
        if (result){
           return result.data;
        }
    } catch (error) {
        throw new Error(error.response.data)
    }
}

export const validate = async (campaignId,status) => {
    try {
        const payload = {"role":"validator","id":"validatorAdmin","args":["validateFundingCampaign",campaignId,status] };
        const result = await axios.post("http://127.0.0.1:3030/newTransaction", payload)
        if (result){
           return result.data;
        }
    } catch (error) {
        throw new Error(error.response.data)
    }
}

export const finalize =  async (reqId) => {
    try {
        const payload = {"role":"fundmanager","id":"FundManagerAdmin","args":["finalize",reqId] };
        const result = await axios.post("http://127.0.0.1:3030/newTransaction", payload);
        if (result){
            return result.data;
        }
        return false
    } catch (error) {
        throw new Error(error.response.data)
    }
}

export const createRequest = async (username,id,description,amount,recepient) => {
    try {
        const payload = {"role":"users","id":"UsersAdmin","args":[username,id,description,amount,recepient] };
        const result = await axios.post("http://127.0.0.1:3030/createRequest", payload);
        if (result){
            return result.data;
        }
        return false
    } catch (error) {
        throw new Error(error.response.data)
    }
}

export const deposit = async (campaignId,username,amount) => {
    try {
        const payload = {"role":"users","id":"UsersAdmin","args":["depositToCampaign",campaignId,username,amount] };
        const result = await axios.post("http://127.0.0.1:3030/newTransaction", payload)
        if (result){
            console.log(result)
            return result.data
        }
        return false
    } catch (error) {
        throw new Error(error.response.data)
    }
}