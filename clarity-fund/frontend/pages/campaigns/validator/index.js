import React, { Component } from 'react';
import { Card, Button, Header } from 'semantic-ui-react';
import { Layout } from '../../../components/Layout'
import { Router } from "../../../routes";
import { allCampaigns, validate } from '../../api'


class CampaignIndex extends Component {
    state = {
        approveLoading: false,
        declineLoading: false,
        currentCampaign: ''
    }
    //by putting static no need to create an Instance
    static async getInitialProps(props) {
        const campaigns = await allCampaigns();
        return { campaigns };
    }
    onApprove = async (id) => {
        this.setState({ approveLoading: true, currentCampaign: id })
        await validate(id, 'active')
        this.setState({ approveLoading: false, currentCampaign: '' })
        Router.pushRoute('/validator')
    }
    onDecline = async (id) => {
        this.setState({ declineLoading: true, currentCampaign: id })
        await validate(id, 'rejected')
        this.setState({ declineLoading: false, currentCampaign: '' })
        Router.pushRoute('/validator')
    }
    renderCampaigns() {
        if (this.props.campaigns.length === 0) {
            return <Header as='h1' textAlign='center'>NO CAMPAIGNS AVAILABLE!</Header>
        }
        let button;
        const items = this.props.campaigns.map(campaignData => {
            if (campaignData.Record.status === 'active') {
                button = (
                    <div className='ui buttons'>
                        <Button positive >ACTIVE</Button>
                        <Button basic color='red' onClick={() => this.onDecline(campaignData.Key)} loading={this.state.declineLoading && (this.state.currentCampaign === campaignData.Key)}>
                            REJECT
                        </Button>
                    </div>
                )
            }
            else if (campaignData.Record.status === 'in Review') {
                button = (
                    <div className='ui two buttons'>
                        <Button basic color='green' onClick={() => this.onApprove(campaignData.Key)} loading={this.state.approveLoading && (this.state.currentCampaign === campaignData.Key)}>
                            APPROVE
                        </Button>
                        <Button basic color='red' onClick={() => this.onDecline(campaignData.Key)} loading={this.state.declineLoading && (this.state.currentCampaign === campaignData.Key)}>
                            DECLINE
                        </Button>
                    </div>
                )
            }
            else (
                button = (
                    <div className='ui buttons'>
                        <Button basic color='green' onClick={() => this.onApprove(campaignData.Key)} loading={this.state.approveLoading && (this.state.currentCampaign === campaignData.Key)}>
                            ACTIVATE
                        </Button>
                        <Button negative>REJECTED</Button>
                    </div>
                )
            )
            return (
                <Card key={campaignData.Key}>
                    <Card.Content>
                        <Card.Header>{campaignData.Key}</Card.Header>
                        <Card.Meta><strong>{campaignData.Record.requirement}</strong></Card.Meta>
                        <Card.Description>
                            <div>Required:{campaignData.Record.amountRequired}</div>
                            <div> Username:{campaignData.Record.username}</div>
                            <div> Address:{campaignData.Record.address}</div>
                            <div> Contact No:{campaignData.Record.contactNo}</div>
                        </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                        {button}
                    </Card.Content>
                </Card>
            )
        });
        return (<Card.Group>
            {items}
        </Card.Group>
        );
    }
    render() {
        return (
            <Layout>
                <div>
                    <Header as="h3">CAMPAIGNS</Header>
                    {this.renderCampaigns()}
                </div>
            </Layout>
        );
    }
}

export default CampaignIndex;