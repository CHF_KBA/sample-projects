import React, { Component } from 'react';
import { Layout } from '../../components/Layout'
import { Card, Grid, Header, Icon, Button, Dimmer, Loader, Segment, Popup } from 'semantic-ui-react';
import { getCampaign } from '../api'
import ContributeForm from '../../components/ContributeForm';
import { Link } from '../../routes';

class CampaignDetails extends Component {
    static async getInitialProps(props) {
        const campaignId = props.query.campaignId
        const campaign = await getCampaign(campaignId)
        const depositors = campaign.depositors
        const approversCount = depositors.length
        return {
            username: campaign.username,
            campaignCollected:campaign.amountCollected,
            campaignBalance: campaign.amount,
            requestedAmount: campaign.amountRequired,
            address: campaign.address,
            contact: campaign.contactNo,
            requirement: campaign.requirement,
            approversCount,
            campaignId
        }
    }
    renderCards() {
        const { username, campaignBalance, requestedAmount, address, contact, requirement, approversCount,campaignCollected } = this.props
        const items = [
            {
                header: username,
                meta: 'Campaign Creator',
                description: `${address},mob:${contact}`,
                style: { overflowWrap: 'break-word' }
            },
            {
                header: campaignBalance+"₹",
                meta: "Campaign Balance (in Rupees)",
                description: `Total Amount Collected by this Campaign till now: ${campaignCollected}₹`
            },
            {
                header: requirement,
                meta: `Requested Amount: ${requestedAmount}₹`,
                description: "This Requirement is verified by Validation Agency"
            },
            {
                header: approversCount,
                meta: 'Number of Approvers',
                description: 'Number of people Contributed for this Campaign'
            }
        ]
        return <Card.Group items={items} />
    }
    render() {
        return (
            <Layout>
                <Link route={`/`}>
                    <a>
                        <Button color='teal' animated>
                            <Button.Content visible>BACK</Button.Content>
                            <Button.Content hidden>
                                <Icon name='arrow left' />
                            </Button.Content>
                        </Button>
                    </a>
                </Link>
                <Header as='h2' block color='grey'>
                    <Icon name='rupee' color='black' />
                    <Header.Content>
                        {this.props.campaignId}
                        <Header.Subheader>
                            <Link route={`/campaigns/${this.props.campaignId}/manager`}>
                                <a>
                                    <Popup
                                        content='Only Restricted to Manger'
                                        inverted
                                        on='hover'
                                        trigger={
                                            <Button content="Manager" icon="user circle" positive />
                                        }
                                    />
                                </a>
                            </Link>
                        </Header.Subheader>
                    </Header.Content>
                </Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={10}>
                            {this.renderCards()}
                        </Grid.Column>
                        <Grid.Column width={6}>
                            <ContributeForm campaignId={this.props.campaignId} />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Link route={`/campaigns/${this.props.campaignId}/requests`}>
                                <a>
                                    <Button primary>View Withdrawal Requests</Button>
                                </a>
                            </Link>
                        </Grid.Column>

                    </Grid.Row>
                </Grid>

            </Layout>
        )
    }
}
export default CampaignDetails