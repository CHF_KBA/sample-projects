import React, { Component } from 'react';
import { Layout } from '../../../components/Layout';
import { Grid, Header, Table, Message, Button, Icon } from 'semantic-ui-react'
import ManagerRow from '../../../components/ManagerRow';
import { getRequests } from '../../api'
import { Link } from '../../../routes'

class RequestIndex extends Component {
    constructor(props) {
        super(props);
        this.setError = this.setError.bind(this)
    }
    static async getInitialProps(props) {
        const { campaignId } = props.query;
        const requests = await getRequests(campaignId)
        const requestCount = requests.length;
        return { campaignId, requests, requestCount }
    }
    state = {
        visible: false,
        errorMessage: ''
    }
    handleDismiss = () => {
        this.setState({ visible: false })
    }
    setError = (message) => {
        if (message === 'Error: transaction returned with failure: Error: This request does not have sufficient approvals,Error: transaction returned with failure: Error: This request does not have sufficient approvals,Error: transaction returned with failure: Error: This request does not have sufficient approvals') {
            this.setState({ errorMessage: 'Request does not have sufficient approval!', visible: true })
        }
        else
            this.setState({ errorMessage: message, visible: true });
    }
    rederMessage() {
        if (this.state.visible) {
            return (
                <Message
                    negative
                    icon='warning circle'
                    onDismiss={this.handleDismiss}
                    header='Error'
                    content={this.state.errorMessage}
                    floating
                />
            )
        }
    }
    renderRows() {
        return this.props.requests.map((request, index) => {
            return <ManagerRow
                setError={this.setError}
                key={index}
                id={index}
                request={request}
                approversCount={request.Record.depositorsCount}
                campaignId={this.props.campaignId}
            />
        })
    }

    render() {
        const { Row, HeaderCell, Body } = Table;
        return (
            <Layout>
                <Link route={`/campaigns/${this.props.campaignId}`}>
                    <a>
                        <Button color='teal' animated>
                            <Button.Content visible>BACK</Button.Content>
                            <Button.Content hidden>
                                <Icon name='arrow left' />
                            </Button.Content>
                        </Button>
                    </a>

                </Link>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={8} >
                            <Header as='h3'>Requset List</Header>
                        </Grid.Column>
                    </Grid.Row>
                    {this.rederMessage()}
                    <Grid.Row>
                        <Table>
                            <Table.Header>
                                <Row>
                                    <HeaderCell>ID</HeaderCell>
                                    <HeaderCell>Description</HeaderCell>
                                    <HeaderCell>Amount</HeaderCell>
                                    <HeaderCell>Recipient</HeaderCell>
                                    <HeaderCell>Approval Count</HeaderCell>
                                    <HeaderCell>Finalize</HeaderCell>
                                </Row>
                            </Table.Header>
                            <Body>
                                {this.renderRows()}
                            </Body>
                        </Table>
                    </Grid.Row>
                    <div>Found {this.props.requestCount} requests</div>
                </Grid>
            </Layout>
        );
    }
}

export default RequestIndex