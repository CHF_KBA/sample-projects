import React, { Component } from 'react'
import { Layout } from '../../components/Layout'
import { Form, Button, Input, Message } from 'semantic-ui-react'
import { Router } from '../../routes';
import { createCampaign } from '../api'

class CampaignNew extends Component {

    state = {
        username: '',
        address: '',
        contact: '',
        requirement: '',
        amount: '',
        errorMessage: '',
        onLoading: false,
        success: false
    };

    onSubmit = async (event) => {
        event.preventDefault(); //to stop browse from submitting form
        try {
            if (!this.state.address || !this.state.amount || !this.state.contact || !this.state.requirement || !this.state.username) {
                throw new Error('Please Enter A Value')
            }
            this.setState({ onLoading: true, errorMessage: '', success: false })
            await createCampaign( this.state.username, this.state.address, this.state.contact, this.state.requirement, this.state.amount);
            this.setState({ success: true })
            Router.pushRoute('/');
        } catch (error) {
            this.setState({
                errorMessage: error.message
            })
        }
        this.setState({ onLoading: false })

    };
    render() {

        return (
            <Layout>
                <h3>Create New Camapign</h3>
                <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage} success={this.state.success}>
                    <Form.Field>
                        <label>Username</label>
                        <Input
                            placeholder='Name'
                            value={this.state.username}
                            onChange={event => this.setState({ username: event.target.value })}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Address</label>
                        <Input
                            placeholder='Address'
                            value={this.state.address}
                            onChange={event => this.setState({ address: event.target.value })}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Contact No.</label>
                        <Input
                            placeholder='Mobile No'
                            value={this.state.contact}
                            onChange={event => this.setState({ contact: event.target.value })}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Reason</label>
                        <Input
                            placeholder='Reason for Funding'
                            value={this.state.requirement}
                            onChange={event => this.setState({ requirement: event.target.value })}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Required Amount</label>
                        <Input
                            placeholder=''
                            value={this.state.amount}
                            onChange={event => this.setState({ amount: event.target.value })}
                        />
                    </Form.Field>
                    <Message
                        success
                        header='Success!'
                        content='You Successfully Created A Camapign'
                    />
                    <Message error header="Oops!" content={this.state.errorMessage} />
                    <Button loading={this.state.onLoading} primary disabled={this.state.onLoading}>
                        Create
                    </Button>
                    {/* primary will give a blue colour for button */}
                </Form>
            </Layout>
        );

    }
}
export default CampaignNew