import React, { Component } from 'react';
import { Layout } from '../../../components/Layout';
import { Button, Grid, Header, Table,Icon } from 'semantic-ui-react'
import { Link } from '../../../routes';
import RequsetRow from '../../../components/RequestRow';
import { getRequests } from '../../api'

class RequestIndex extends Component {
    static async getInitialProps(props) {
        const { campaignId } = props.query;
        const requests = await getRequests(campaignId)
        const requestCount = requests.length;
        return { campaignId, requests, requestCount }
    }
    renderRows() {
        return this.props.requests.map((request, index) => {
            return <RequsetRow
                key={index}
                id={index}
                request={request}
                approversCount={request.Record.depositorsCount}
                campaignId={this.props.campaignId}
            />
        })
    }

    render() {
        const { Row, HeaderCell, Body } = Table;
        return (
            <Layout>
                <Link route={`/campaigns/${this.props.campaignId}`}>
                    <Button color='teal' animated>
                        <Button.Content visible>BACK</Button.Content>
                        <Button.Content hidden>
                            <Icon name='arrow left' />
                        </Button.Content>
                    </Button>
                </Link>
        
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={8} >
                            <Header as='h3'>Request List</Header>
                        </Grid.Column>
                        <Grid.Column width={8}>
                            <Link route={`/campaigns/${this.props.campaignId}/requests/new`}>
                                <a>
                                    <Button primary floated="right">Add Request</Button>
                                </a>
                            </Link>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Table>
                            <Table.Header>
                                <Row>
                                    <HeaderCell>ID</HeaderCell>
                                    <HeaderCell>Description</HeaderCell>
                                    <HeaderCell>Amount</HeaderCell>
                                    <HeaderCell>Recipient</HeaderCell>
                                    <HeaderCell>Approval Count</HeaderCell>
                                    <HeaderCell>Approve</HeaderCell>
                                </Row>
                            </Table.Header>
                            <Body>
                                {this.renderRows()}
                            </Body>
                        </Table>
                    </Grid.Row>
                    <div>Found {this.props.requestCount} requests</div>
                </Grid>
            </Layout>
        );
    }
}

export default RequestIndex