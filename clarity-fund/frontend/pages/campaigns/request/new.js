import React, { Component } from 'react';
import { Layout } from '../../../components/Layout'
import { Form, Message, Input, Button, Header, Grid,Icon } from 'semantic-ui-react'
import { createRequest } from '../../api'
import { Router, Link } from '../../../routes'


class NewRequest extends Component {
    static async getInitialProps(props) {
        const campaignId = props.query.campaignId;
        return { campaignId }
    }
    state = {
        username: '',
        description: '',
        amount: '',
        account: '',
        success: false,
        errorMessage: '',
        onLoading: false
    }
    onSubmit = async (event) => {
        try {
            if (!this.state.description || !this.state.amount || !this.state.account || !this.state.username) {
                throw Error("Please Complete the form")
            }
            event.preventDefault();
            this.setState({ onLoading: true })
            const result = await createRequest(this.state.username, this.props.campaignId, this.state.description, this.state.amount, this.state.account);
            if (result) {
                this.setState({ onLoading: false })
                this.setState({ success: true })
                Router.pushRoute(`/campaigns/${this.props.campaignId}/requests`);
            } else {
                this.setState({ errorMessage: 'something went wrong' })
                Router.pushRoute(`/campaigns/${this.props.campaignId}/requests`);
            }
        } catch (error) {
            this.setState({ errorMessage: error.message })
        }
        this.setState({ onLoading: false })
    }
    render() {
        return (
            <Layout>
                <Link route={`/campaigns/${this.props.campaignId}/requests`}>
                    <Button color='teal' animated>
                        <Button.Content visible>BACK</Button.Content>
                        <Button.Content hidden>
                            <Icon name='arrow left' />
                        </Button.Content>
                    </Button>
                </Link>
                <Header as="h3">Create A Request</Header>
                <Grid>
                    <Grid.Column floated='left' width={8}>
                        <Form onSubmit={this.onSubmit} success={this.state.success} error={!!this.state.errorMessage}>
                            <Form.Field>
                                <label>Username</label>
                                <Input
                                    value={this.state.username}
                                    onChange={event => this.setState({ username: event.target.value, errorMessage: '' })}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>Description</label>
                                <Input
                                    value={this.state.description}
                                    onChange={event => this.setState({ description: event.target.value, errorMessage: '' })}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>Amount</label>
                                <Input
                                    value={this.state.amount}
                                    label='RUP'
                                    labelPosition='right'
                                    onChange={event => this.setState({ amount: event.target.value, errorMessage: '' })}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>Recipient Account</label>
                                <Input
                                    value={this.state.account}
                                    onChange={event => this.setState({ account: event.target.value, errorMessage: '' })}
                                />
                            </Form.Field>
                            <Message
                                success
                                header='Sucess!'
                                content='Sucessfully created Request'
                                icon='check'
                            />
                            <Message error icon="times circle outline" header="Oops!" content={this.state.errorMessage} />
                            <Button icon="add circle" primary content="Create" loading={this.state.onLoading} disabled={this.state.onLoading} />
                        </Form>
                    </Grid.Column>
                </Grid>
            </Layout>
        )
    }
}

export default NewRequest