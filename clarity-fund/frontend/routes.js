const routes = require('next-routes')();

routes 
    .add('/validator','/campaigns/validator/index')
    .add('/campaigns/new','/campaigns/new')
    .add('/campaigns/:campaignId','/campaigns/details' )
    .add('/campaigns/:campaignId/requests','/campaigns/request/index')
    .add('/campaigns/:campaignId/requests/new','/campaigns/request/new')
    .add('/campaigns/:campaignId/manager','/campaigns/manager/index')

module.exports = routes
