# CLARITYFUND:FRONTEND
### **This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).**

## Getting Started

**Install the node packages** 

* npm install

**then, run the server:**

* npm run start


Open [Clarity Fund](http://localhost:5000 "HOME") with your browser to see the pages.






