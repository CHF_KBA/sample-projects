import React, { Component } from 'react';
import { Table, Button, Popup, Progress, Icon, Form, Input, Message } from 'semantic-ui-react'
import { Router } from '../routes';
import { approve, finalize } from '../pages/api'


class RequestRow extends Component {

    state = {
        approved: false,
        appovalLoading: false,
        finalizeLoading: false,
        username: '',
        requestId: '',
        errorMessage: '',
        approveSuccess:false
    }

    onApprove = async () => {
        try {
            if (!this.state.username) {
                throw new Error('Please Enter A Value')
            }
            this.setState({ appovalLoading: true })
            const approval = await approve(this.state.requestId, this.state.username)
            if (approval) {
                this.setState({ approved: true,appovalLoading: false,approveSuccess:true })
                Router.replaceRoute(`/campaigns/${this.props.campaignId}/requests`)
            }
            else{
                this.setState({ approved: false,appovalLoading: false,approveSuccess:false })
            }
        } catch (error) {
            if (error.message === 'Error: transaction returned with failure: Error: This user does not have permission to aprrove this request,Error: transaction returned with failure: Error: This user does not have permission to aprrove this request,Error: transaction returned with failure: Error: This user does not have permission to aprrove this request'){
                this.setState({ errorMessage: 'Un Authorized' })
            }else if(error.message === 'Error: transaction returned with failure: Error: This user has already approved this request,Error: transaction returned with failure: Error: This user has already approved this request,Error: transaction returned with failure: Error: This user has already approved this request'){
                this.setState({ errorMessage: 'Already Approved' })
            }
            else{
                this.setState({ errorMessage: error.message })
            }
            this.setState({ approved: false,appovalLoading: false,approveSuccess:false })
            
        }

    }

    render() {
        const { Row, Cell } = Table
        const { id, request } = this.props
        const readyToFinalize = request.Record.approvals.length > request.Record.depositorsCount / 2;

        return (
            <Row disabled={request.Record.isComplete} positive={readyToFinalize && !request.Record.isComplete}>
                <Cell>{(id) + 1}</Cell>
                <Cell>{request.Record.description}</Cell>
                <Cell>{request.Record.amount}</Cell>
                <Cell>{request.Record.account}</Cell>
                <Cell>
                    {request.Record.isComplete ? (
                        <Progress percent={100} success>
                            Completed
                        </Progress>
                    ) : (
                            <Progress value={request.Record.approvals.length} total={request.Record.depositorsCount} active color='yellow'>
                                {request.Record.approvals.length}/{request.Record.depositorsCount}
                            </Progress>
                        )
                    }
                </Cell>
                <Cell >
                    {request.complete ? (
                        <Icon name='check circle' size='big' />
                    ) : (
                            this.state.approved ? (
                                <Button color='green'>Approved</Button>
                            ) : (
                                    <Popup trigger={<Button>Action</Button>} flowing hoverable>
                                        <Form error={!!this.state.errorMessage} success={this.state.approveSuccess}>
                                            <Form.Field>
                                                <label>Username</label>
                                                <Input
                                                    placeholder='Name'
                                                    value={this.state.username}
                                                    onChange={event => this.setState({ username: event.target.value, requestId: request.Key,errorMessage: '' })}
                                                />
                                            </Form.Field>
                                            <Message success
                                                icon="check"
                                                header='Success!'
                                                content={`You Successfully Approved ${request.Key}`} />
                                            <Message error icon="times circle outline" header="Oops!" content={this.state.errorMessage} />
                                            <Button loading={this.state.appovalLoading} primary disabled={this.state.appovalLoading} onClick={this.onApprove}>
                                                Approve
                                            </Button>
                                        </Form>
                                    </Popup>
                                )
                        )
                    }
                </Cell>
            </Row>
        )
    }
}

export default RequestRow