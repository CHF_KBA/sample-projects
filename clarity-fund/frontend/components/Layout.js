import React from 'react';
import { Container, Header,Icon } from 'semantic-ui-react'
import CampaignHeader from './CampaignHeader'
import Head from 'next/head'

export const Layout = (props) => {
    return (
        <Container>
            <Head>
                <title>CLARITY FUND</title>
                <link rel="shortcut icon" href="/favicon.ico" />
                <link
                    rel="stylesheet"
                    href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"
                />
            </Head>
            <div>
                <Header as='h2' icon textAlign='center' color='blue'>
                    <Icon name='qrcode' circular />
                    <Header.Content>CLARITY FUND</Header.Content>
                </Header>
            </div>
            <CampaignHeader />
            {props.children}
        </Container>
    );
}