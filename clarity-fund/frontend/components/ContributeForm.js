import React, { Component } from 'react';
import { Form, Input, Message, Button, Icon } from 'semantic-ui-react';
import { Router } from '../routes';
import { deposit } from '../pages/api'

class ContributeForm extends Component {
    state = {
        username: '',
        value: '',
        errorMessage: '',
        onLoading: false,
        success: false
    };
    onSubmit = async (event) => {
        try {
            if (!this.state.value || !this.state.username) {
                throw new Error('Please Enter A Value')
            }
            event.preventDefault();
            this.setState({ onLoading: true })
            const result = await deposit(this.props.campaignId, this.state.username, this.state.value)
            if (result) {
                this.setState({ success: true })
                Router.replaceRoute(`/campaigns/${this.props.campaignId}`)
            }
            else {
                this.setState({ success: false })
            }
        } catch (error) {
            this.setState({ errorMessage: error.message })
        }
        this.setState({ onLoading: false })
    }
    render() {
        return (
            <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage} success={this.state.success}>
                <Form.Field>
                    <label>Username</label>
                    <Input
                        value={this.state.username}
                        onChange={event => this.setState({ username: event.target.value, success: false, errorMessage: '' })}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Amount to Contribute</label>
                    <Input
                        value={this.state.value}
                        label='RUP'
                        labelPosition="right"
                        onChange={event => this.setState({ value: event.target.value, success: false, errorMessage: '' })}
                    />
                </Form.Field>
                <Message
                    success
                    icon="check"
                    header='Success!'
                    content={`You Successfully Contributed to ${this.props.campaignId}`}
                />
                <Message error icon="times circle outline" header="Oops!" content={this.state.errorMessage} />
                <Button loading={this.state.onLoading} disabled={this.state.onLoading} primary>Contribute!</Button>
            </Form>
        );
    }
}
export default ContributeForm