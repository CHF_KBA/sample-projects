import React, { Component } from 'react';
import { Table, Button, Progress, Icon, Message } from 'semantic-ui-react'
import { Router } from '../routes';
import { finalize } from '../pages/api'


class ManagerRow extends Component {

    state = {
        approved: false,
        appovalLoading: false,
        finalizeLoading: false,
        username: '',
        requestId: '',
        onLoading: false
    }

    onFinalize = async () => {
        try {
            this.setState({ finalizeLoading: true })
            const result = await finalize(this.props.request.Key);
            if (result) {
                this.setState({ finalizeLoading: false })
                Router.replaceRoute(`/campaigns/${this.props.campaignId}/manager`)
            }
        } catch (error) {
            this.setState({ finalizeLoading: false })
            this.props.setError(error.message)
        }

    }
    render() {
        const { Row, Cell } = Table
        const { id, request } = this.props
        const readyToFinalize = request.Record.approvals.length > request.Record.depositorsCount / 2;
            return (
                <Row disabled={request.Record.isComplete} positive={readyToFinalize && !request.Record.isComplete}>
                    <Cell>{(id) + 1}</Cell>
                    <Cell>{request.Record.description}</Cell>
                    <Cell>{request.Record.amount}</Cell>
                    <Cell>{request.Record.account}</Cell>
                    <Cell>
                        {request.Record.isComplete ? (
                            <Progress percent={100} success>
                                Completed
                            </Progress>
                        ) : (
                                <Progress value={request.Record.approvals.length} total={request.Record.depositorsCount} active color='yellow'>
                                    {request.Record.approvals.length}/{request.Record.depositorsCount}
                                </Progress>
                            )
                        }
                    </Cell>
                    <Cell textAlign='center'>
                        {request.complete ?
                            (
                                <Icon name='check circle' size='big' />
                            ) :
                            (
                                <Button
                                    color='red'
                                    basic
                                    loading={this.state.finalizeLoading}
                                    disabled={this.state.finalizeLoading}
                                    onClick={this.onFinalize}
                                >
                                    Finalize
                                </Button>

                            )
                        }
                    </Cell>
                </Row>
            )
    }
}

export default ManagerRow