import React, { useState } from 'react';
import { Menu, Button, Icon, Dimmer, Loader, Container } from 'semantic-ui-react';
import { Link ,Router} from '../routes'
import { useRouter } from "next/router";


const campaignHeader = (props) => {
    const [onLoading, setOnLoading] = useState(false)
    const router = useRouter()
    const handleHomeLoading =()=>{
        if(router.pathname !== '/'){
            Router.pushRoute('/');
        }
    }
    const handleValidatorLoading =()=>{
        if(router.pathname !== 'validator' && router.pathname !== '/campaigns/validator'){
            Router.pushRoute('/validator');
            
        }
    }
    Router.onRouteChangeStart = () => {
        setOnLoading(true)
      };
      
      Router.onRouteChangeComplete = () => {
        setOnLoading(false)
      };
      
      Router.onRouteChangeError = () => {
        setOnLoading(false)
      };
    if (onLoading) {
        return (
            <Container>
            <Menu style={{ marginTop: '40px' }}>
                    <a className='item'>Home <span /> <Icon name="home" /></a>
                <Menu.Menu >
                </Menu.Menu>
                <Menu.Menu position="right">
                        <a className='item'>
                            <Button floated="right" content="Validator" icon="shield alternate" secondary />
                        </a>
                </Menu.Menu>
            </Menu>
            <Dimmer active inverted>
                <Loader>Loading</Loader>
            </Dimmer>
            </Container>
        )
    } else {
        return (
            <Menu style={{ marginTop: '40px' }}>
                    <a className='item' onClick={()=>handleHomeLoading()}>Home <span /> <Icon name="home" /></a>
                <Menu.Menu position="right">
                        <a className='item'>
                            <Button floated="right" content="Validator" onClick={()=>handleValidatorLoading()} icon="shield alternate" secondary  />
                        </a>
                </Menu.Menu>
            </Menu>

        );
    }

}

export default campaignHeader