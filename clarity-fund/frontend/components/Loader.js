import React from 'react';
import { Dimmer, Loader } from 'semantic-ui-react'

export const Loader = (props) => {
    return (
        <Layout>
            <Dimmer active>
                <Loader content='Loading' />
            </Dimmer>
        </Layout>
    );
}