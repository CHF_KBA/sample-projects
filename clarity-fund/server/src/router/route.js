'use strict';
import { submitTransaction,createCampaign,createRequest } from '../services/transaction';
import { logger } from '../utils/logger';

export default (app) => {
    try {
        app.route('/newTransaction').post(submitTransaction);
        app.route('/createCampaign').post(createCampaign);
        app.route('/createRequest').post(createRequest)
    } catch (error) {
        logger.error(error)
    }

}