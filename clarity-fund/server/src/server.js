'use strict';
import express from 'express';
import routes from './router/route';
import config from '../config'
import errorHandler from 'express-error-handler';
import logger from './utils/logger';
import cors from 'cors';

require('log-timestamp');


var app = express();
app.use(express.json());
app.use(cors());
const port = config.port;

const server = app.listen(port);
server.on('listening', () =>
  console.log(`server Started at http://${config.host}:${port}`)
)
routes(app)
app.use(errorHandler({ logger }))
app.use(function (req, res) {
  res.status(404).send({ error: 'not found' });
});
