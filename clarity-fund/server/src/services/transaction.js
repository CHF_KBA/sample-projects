'use strict'
import { logger } from '../utils/logger';
import { clientApplication } from '../client'
import uniqid from 'uniqid'


export const submitTransaction = async (req, res) => {
    try {
        const { role,id,args } = req.body;
        let clientApp = new clientApplication();
        clientApp.setRoleAndIdentity(role,id);
        clientApp.initChannelAndChaincode("autochannel","clarityfund");
        const result =  await clientApp.generatedAndSubmitTxn(...args)
        return res.status(200).send(result)
    } catch (error) {
        logger.error(error);
        return res.status(400).send(error.message)
    }
}

export const createCampaign = async (req, res) => {
    try {
        const { role,id,args } = req.body;
        let clientApp = new clientApplication();
        clientApp.setRoleAndIdentity(role,id);
        clientApp.initChannelAndChaincode("autochannel","clarityfund");
        const campId = uniqid('camp')
        const result =  await clientApp.generatedAndSubmitTxn('createFundingCampaign',campId,...args)
        if(result){
            return res.status(200).send(result)
        }
        return res.status(400).send('error creating campaign')
    } catch (error) {
        logger.error(error);
        return res.status(400).send(error.message)
    }
}

export const createRequest = async (req, res) => {
    try {
        const { role,id,args } = req.body;
        let clientApp = new clientApplication();
        clientApp.setRoleAndIdentity(role,id);
        clientApp.initChannelAndChaincode("autochannel","clarityfund");
        const reqId = uniqid('req')
        console.log(reqId,...args,'consiling')
        const result =  await clientApp.generatedAndSubmitTxn('createWithdrawalRequest',reqId,...args)
        return res.status(200).send(result)
    } catch (error) {
        logger.error(error);
        return res.status(400).send(error.message)
    }
}