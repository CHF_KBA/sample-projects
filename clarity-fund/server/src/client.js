
import { profile } from './profile'
import { FileSystemWallet, Gateway } from 'fabric-network';
import { logger } from './utils/logger'



export class clientApplication {

    async setRoleAndIdentity(role, identityLabel) {
        this.Profile = profile[role.toLowerCase()]
        let wallet = new FileSystemWallet(this.Profile["Wallet"])
        this.connectionOptions = {
            identity: identityLabel,
            wallet: wallet,
            discovery: { enabled: true, asLocalhost: true }
        }


    }

    initChannelAndChaincode(channelName, contractName) {
        this.channel = channelName
        this.contractName = contractName
    }

    async generatedAndSubmitTxn(txnName, ...args) {
        let gateway = new Gateway()
        try {
            await gateway.connect(this.Profile["CCP"], this.connectionOptions);
            let channel = await gateway.getNetwork(this.channel);
            let contract = await channel.getContract(this.contractName)
            let result = await contract.submitTransaction(txnName, ...args);
            if (result.toString()) {
                return JSON.parse(result.toString());
            }
        } catch (error) {
            logger.error(`Error processing transaction. ${error}`);
            logger.error(error.stack);
            throw Error(error.endorsements)

        } finally {
            logger.info('Disconnected from Fabric gateway.');
            gateway.disconnect();

        }
    }
}