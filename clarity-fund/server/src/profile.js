let profile = {

    fundmanager: {

        "Wallet": "./../network/wallets/FundManager",

        "CCP": "./../network/gateways/FundManager/FundManager gateway.json"

    },

    validator: {

        "Wallet": "./../network/wallets/Validator",

        "CCP": "./../network/gateways/Validator/Validator gateway.json"

    },

    users: {
        "Wallet": "./../network/wallets/Users",

        "CCP": "./../network/gateways/Users/Users gateway.json"
    }


}

module.exports = {

    profile
}