require('dotenv').config();
var config = require('12factor-config')

var cfg = config({
    host: {
        env: 'APP_HOST',
        type: 'string',
        default: '127.0.0.1',
        required: true
    },
    port: {
        env: 'APP_PORT',
        type: 'integer',
        default: 3030,
        required: true
    },
    env: {
        env: 'NODE_ENV',
        type: 'enum',
        default: 'development',
        required: true,
        values: ['development', 'production', 'test'],
    }

})
module.exports = cfg