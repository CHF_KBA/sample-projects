# CLARITYFUND:BACKEND
### **THIS IS THE BACKEND SERVER FOR CLARITY FUND AND IT WILL PROVIDE REST API SERVICES FOR EXECUTING TRANSACTIONS**

## Getting Started

**Install the node packages**

* npm install 

Then start the server,

* npm run start:dev

**The server will listen at [http://localhost:3030]**

