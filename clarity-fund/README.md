# CLARITY FUND: A BLOCKCHAIN SOLUTION FOR TRANSPARENT FUND COLLECTION AND MANAGEMENT

**Done as a part of KBA-CHF 202 by AKSHAY DEV H & REKHA KIRAN**

***
## Table of Contents

-   [**About The Project**](#about-the-project)
    -   [Problem](#-problem)
    -   [Solution](#-solution)
-   [**Workflow**](#workflow)
-   [**Running the Application**](#running-the-application)
    -   [Prerequisites](#-prerequisites)
    -   [Installation](#-installation)
    -   [How to run the Application according to the workflow](#-how-to-run-the-application-according-to-the-workflow)
-   [**Chaincode Functions**](#chaincode-functions)
-   [**Resources and technologies Used**](#resources-and-technologies-used)

## About The Project

_"CLARITY FUND: A BLOCKCHAIN SOLUTION FOR TRANSPARENT FUND COLLECTION AND MANAGEMENT "_

Trust & Transparency are the most important parameters when a crowdfund is concerned, both in the donors' and the beneficiaries' point of view. Most of the current systems lack these perspectives. Everything remains secretive and hence apprehensive too. Our project aims to solve these issues with the current system by incorporating Blockchain technology( Hyperledger Fabric). Hyperledger Fabric is a permissioned blockchain, capable of transparent transactions.

### • Problem

There are several types of crowdfunding (donation based, rewards based, debt, equity) but not all types of crowdfunding are regulated by government organizations . Crowdfunding has many benefits, but it also comes with a substantial risk of fraud -- as well as a number of potential risks, such as loss of money, locked-in investment, and lack of information. Many people worry about being scammed by a fraudulent request, due to crowdfunding's lack of transparency and trust. There have been few reported cases of fraud in crowdfunding -- particularly when compared to the number of transactions and amounts involved -- however as the number of crowdfunding platforms grows, so does the risk that platform operators themselves may engage in or enable fraud.

The main problems with the current system are as follows

         • There will not be any cap on the required amount or specific time period for donations.
         • No auditing of the collected Funds.
         • Depositors does not have any  Control over their Funds.
         • Lack of transparency in fund   management.
         • Middleman has complete rights over the collected funds. He/She can decide how to spend the funds.
         • Centralized execution.

### • Solution

A Blockchain based crowdfund network can enable transparent and efficient funding system.

    • The fund is managed by pre-written smart-contracts. This eliminates the need of a middle man.
    • Cap on the amount and limiting campaign time period can be easly achieved by smart-contracts.
    • Donors have complete rights over their funds and they can decide how to spend their money through voting.
    • Each and every transaction are  recorded in a decentralized ledger. Hence it becomes immutable and transparent.
    • Social auditing is possible.
    • Confidential data can be hidden from general public through channels or PDC.
    • Decentralized

## Workflow

**Asset:**
Funding Campaign

**Participants:**  
1.Fund manager  
2.Validator  
3.Users (Campaign Owner/Donors)

    • There will be 3 organizations in the network. Fund Manager, Validator and Users
    • Users (Campaign Owner) can create a campaign request for raising funds by providing the detailed description of their requirement, estimated amount etc. (Campaign)
    • Validator will  validate the campaign by contacting the Campaign Owner. If the validator thinks that the campaign is genuine, he/she can mark the campaign as ACTIVE.
    • Public (Donors) can visit the website, check all the active campaigns.
    • Those who donate  will get right to vote on that particular campaign.
    • For withdrawing the collected fund, the campaign owner must create a withdrawal request which includes details of withdrawal. They have to specify the exact reason for the withdrawal and how much amount they required for that particular reason.
    • Those depositors with voting rights can now check withdrawal request and if they are convinced with the reason they can approve the withdrawal request.
    • If any withdrawal request passes the threshold (51%) approval. It is eligible for withdrawal. The fund manager can now finalize the withdrawal.

![work flow](images/workflow.png)

## Running the Application

### • Prerequisites

Install the Visual Studio Code IDE and IBM Blockchain Platform Extension.

**Installing Dependencies**

1.  Clone the repository and Open the shell script directory

            $ git clone https://gitlab.com/akshaydevh/clarity-fund.git
            $ cd scripts/

_(skip all other steps if you already installed dependencies)_

2.  Provide execute permission for the script using the command

           $ chmod +x installDependencies.sh

3.  To Install the dependencies execute the command

          $ ./installDependencies.sh

This will prompt you for providing the user password , give the password .

4.  When the dependency installation is complete reboot your machine.

5.  Now execute the command

           $ ./installDependencies.sh bin

This will download all the hyperledger binaries and docker images required for the setup.

### • Installation

( _make sure all the hyperledger binaries and docker images are installed_ )

**Bootstrap the Fabric network using Ansible Playbook**

1.  Open the network directory from root.

         $ cd network/

2.  Setup and install role from Ansible Galaxy.

         $ ansible-galaxy install -r requirements.yml --force

3.  Starting the CLARITY FUND NETWORK

         $ ansible-playbook playbook.yml

**Deploying Chaincode**

Open IBM Blockchain Platform extension in VSCode at chaincode directory

-   package the chaincode
-   connect to the Fabric Environment ( Add the Ansible-created network )
-   install and instatiate the chaincode to all node (clarityfund@1.0.0)

**Start the Backend API Server**

1.  Open Server directory

         $ cd server/

2.  Install node modules

         $ npm install

3.  Start the server

         $ npm run start:dev

_The server will listen at [http://localhost:3030]._

**Start the Frontend Server**

1.  Open Frontend directory

         $ cd frontend/

2.  Install node modules

         $ npm install

3.  Start the server

         $ npm run start

**Open [Clarity Fund](http://localhost:5000 "HOME") with your browser to see the pages.**

### • How to run the Application according to the workflow

**Step1:**

[Current Role: User]

![screenshot1](images/Screenshot01.png)

Open the home page (http://localhost:5000) on your browser and create a new funding campaign by clicking 'Create Campaign' button.

**Step2:**

[Current Role: User]

![screenshot2](images/Screenshot02.png)

Fill the details and press 'Create' button.Make sure that you do remember the username (Campaign Owner).

**Step3:**

[Current Role: Validator]

![screenshot3](images/Screenshot03.png)

Come back to the Home page and click the 'Validator' button.

**Step4:**

[Current Role: User]

![screenshot4](images/Screenshot04.png)

Click the 'APPROVE' button to activate the Campaign.

**Step5:**

[Current Role: User]

![screenshot5](images/Screenshot05.png)

Our newly created Campaign is active now and it will be listed on Home page. Click the 'Contribute' button for donating to the Campaign.

**Step6:**

[Current Role: User]

![screenshot6](images/Screenshot06.png)

Fill the details of the contribute form and click 'Contribute!'. Please do remember the usernames used for contributing (depositors).

Two usernames are used to contribute in this example ('akshay','amith').

**Step7:**

[Current Role: User]

![screenshot7](images/Screenshot07.png)

Click the 'View Withdrawal Request'.

**Step8:**

[Current Role: User]

![screenshot8](images/Screenshot08.png)

Click the 'Add Request' button for creating new Withdrawal Request.

**Step9:**

[Current Role: User]

![screenshot9](images/Screenshot09.png)

Fill the details in the requset form.

Make sure that you use the same username that used for creating Campaign (Campaign Owner). Specify the exact reason for this withdrawal, Amout required and Recepient Bank Account Number.

Now Click the 'Create' button.

**Step10:**

[Current Role: User]

![screenshot10](images/Screenshot10.png)

All withdrawl requests will be listed here.

Click the 'Action' button. A voting form will pop-up. Give the depositors username (Refer step 6) and click 'Approve'.

![screenshot11](images/Screenshot11.png)

Make sure the request get 51% (morethan half) approvals.

![screenshot12](images/Screenshot12.png)

After sufficient vote click 'Back' button.

_(Refresh page for adding more approvals)._

**Step11:**

[Current Role: User]

![screenshot13](images/Screenshot13.png)

Click the 'Manager' button in the Campaign Details page.

**Step12**

[Current Role: Fund Manager]

![screenshot14](images/Screenshot14.png)

Click the 'Finalize' button. This will deduct amount from the Campaign.

![screenshot15](images/Screenshot15.png)

check the Campaign details for verification.

## Chaincode Functions

-   **IDExists**: To check whether campaign's id,requests id etc exists.

-   **createFundingCampaign** : To create new funding campaign.

-   **readFundingCampaign** : To read the campaign details by giving funding campaign id.

-   **validateFundingCampaign** : To update the funding campaign status (active/ reject). This function is restricted to validatorMSP only( by validator role only).

-   **createWithdrawalRequest** : This function helps to Create a request with neceessary details to withdraw funds from campaign.

-   **depositToCampaign** : Function to donate/deposit amount to a campaign.

-   **approveRequest** : After creating the campaign, those who have donated towards it can approve the withdrawal request by voting. This function helps to cast depositors approval/vote. It also verifies whether a depositor already approved or not.

-   **getRequests**: This function is used to fetch all withdrawal request related to a campaign. Rich Query is used get data from Database.

-   **finalize** : This function helps Fund Manger to finalize a Withdrawal Request by releasing requested amount into Recipients account. However this can be done if and only if the approval(voting) percent crossed the minimum threshold (51% of the all depositors).After finalising, the requested amount is reduced from the total amount collected by the campaign. This function is restricted to fundManager MSP.

-   **getAllcampaign**: This function is used to get all active campaigns and get all campaigns.

-   **getAllresults**: To format the query results.

-   **DeleteFundingCampaign** : To delete existing campaigns.

## Resources and technologies Used

-   Hyperledger Fabric
-   Couch DB
-   IBM Blockchain Platform
-   Docker
-   Ansible
-   Nodejs
-   Express.js
-   Next.js
-   Visual Studio Code IDE

## Right of Ownership

The ownership of this project is underlined with KBA and should not be used/reproduced anywhere without the permission of the institute. 
