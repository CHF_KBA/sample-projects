#!/bin/bash
#
# SPDX-License-Identifier: Apache-2.0
#


echo "   __ __ ___   ___ ";
echo "  / //_// _ ) / _ |";
echo " / ,<  / _  |/ __ |";
echo "/_/|_|/____//_/ |_|";
echo "                   ";

echo "   ____ __              __ ";
echo "  / __// /_ ___ _ ____ / /_";
echo " _\ \ / __// _ \`// __// __/";
echo "/___/ \__/ \_,_//_/   \__/ ";
echo "                           ";

function nextStep() {
  echo "To install Hyperledger fabric binaries use.... " 
  echo "./InstallAllDependencies.sh bin "
  echo "      - 'bin' - Download and copies all required binaries, also pull required docker images "
}

installAnsible(){
    echo -e "Checking if Ansible is installed"
    local return_=1
    type ansible >/dev/null 2>&1 || { local return_=0; }
    if [ $return_ -eq 0 ]; then 
        printf "\e[91m✘ ansible is not installed \\n"
        echo "Updating repository "
        sudo apt install software-properties-common -y
        sudo apt-add-repository ppa:ansible/ansible -y 
        sudo apt update -y 
        echo "Installing ansible......."
        sudo apt install ansible -y
        printf "\e[32m✔ ansible installed \\n \\n" 
    else 
        type ansible
        printf "\e[34m✔ ansible already installed \\n \\n"
    fi

    }

installDocker(){
    echo "Checking if Docker is installed \\n"
    local return_=1
    type docker >/dev/null 2>&1 || { local return_=0; }
    if [ $return_ -eq 0 ]; then
        printf "\e[91m✘ docker is not installed \\n"
        echo "Getting Docker"
        sudo apt install curl -y
        curl -fsSL https://get.docker.com -o get-docker.sh
        chmod +x get-docker.sh
        echo "Installing Docker"
        ./get-docker.sh
        echo "Installing docker-compose"
        sudo apt install docker-compose -y
        rm get-docker.sh
        printf "\e[32m✔ docker installed \\n \\n"

    else 
        type docker
        printf "\e[34m✔ docker already installed\\n \\n"
    fi
}

installGo(){
    echo "Checking if golang is installed"
    local return_=1
    type go >/dev/null 2>&1 || { local return_=0; }
    if [ $return_ -eq 0 ]; then
        printf "\e[91m✘ go is not installed"
        echo "Installing wget"
        sudo apt install wget -y
        echo "starting golang installation"
        wget https://dl.google.com/go/go1.13.3.linux-amd64.tar.gz
        sudo tar -xvf go1.13.3.linux-amd64.tar.gz
        sudo mv go /usr/local
        echo "export GOROOT=/usr/local/go" >> ~/.profile
        echo "export GOPATH=$HOME/go" >> ~/.profile
        echo "export PATH=$PATH:/usr/local/go/bin" >> ~/.profile
        cd ~
        sleep 3
        source ~/.profile
        type go
        printf "\e[32m✔ go installed \\n \\n"
    else 
        type go
        printf "\e[34m✔ go already installed \\n \\n"
    fi
}


installJq(){
    echo "Checking if jq is installed"
    local return_=1
    type jq >/dev/null 2>&1 || { local return_=0; }
    if [ $return_ -eq 0 ]; then
        printf "\e[91m✘ jq is not installed"
        echo "Installing jq"
        sudo apt install jq -y
        echo "Installed jq"
        type jq
        printf "\e[32m✔ jq installed \\n \\n"
    else 
        type jq
        printf "\e[34m✔ jq already installed \\n \\n"
    fi
}

installSponge(){
    echo "Checking if sponge is installed"
    local return_=1
    type sponge >/dev/null 2>&1 || { local return_=0; }
    if [ $return_ -eq 0 ]; then
        printf "\e[91m✘ sponge is not installed"
        echo "Installing Sponge"
        sudo apt install moreutils -y
        echo "Installed Sponge"
        type sponge
        printf "\e[32m✔ sponge installed \\n \\n"
    else 
        type sponge
        printf "\e[34m✔ sponge already installed \\n \\n"
    fi
}
installNodeJs(){
    echo "Checking if node.js is installed"
    local return_0=1
    type nodejs >/dev/null 2>&1 || { local return_0=0; }
    if [ $return_0 -eq 0 ]; then
        printf "\e[91m✘ nodejs is not installed"
        echo "Installing curl"
        sudo apt install curl -y
        cd ~
        curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
        sudo bash nodesource_setup.sh
        echo "Installing nodejs version 10"
        sudo apt install nodejs -y
        nodejs -v
        type nodejs
        printf "\e[32m✔ nodejs installed \\n"
    else 
        type nodejs
        printf "\e[34m✔ nodejs already installed \\n \\n"
    fi
}


installNpm(){
    echo "Checking if npm is installed"
    local return_0=1
    type npm >/dev/null 2>&1 || { local return_0=0; }
    if [ $return_0 -eq 0 ]; then
        printf "\e[91m✘ npm is not installed"
        type nodejs
        sudo apt install npm -y
        npm -v
        type npm
        printf "\e[32m✔ npm installed \\n \\n "
    else 
        type npm
        printf "\e[34m✔ npm already installed \\n \\n"
    fi


}


installHlfBinaries(){
    cd /home/$USER/
    echo "Fetching Hyperledger binaries "
    curl -sSL http://bit.ly/2ysbOFE | bash -s -- 1.4.7 1.4.7 0.4.21
    echo "Downloaded the fabric-samples folder and placed on /home/$USER"
    echo "Copying binaries"
    sudo cp fabric-samples/bin/* /usr/local/bin
    echo "export PATH=$PATH:/usr/local/bin" >> ~/.profile
    source ~/.profile
    echo "Checking proper installation"
    echo -e "\e[1mFabric Binaries "
    
    peer version 
    
    orderer version
    
    
    echo -e "\e[1mFabric CA Binaries \e[25m \\n"
    
    fabric-ca-server version
    
    fabric-ca-client version
    
    printf "\e[32m✔ Installed Hyperledger fabric binaries \\n"

}

installDependencies(){
    if [[ $1 == "bin" ]]; then 
        installHlfBinaries
        echo -e "Installation Successfull"
        echo "   ____         __";
        echo "  / __/___  ___/ /";
        echo " / _/ / _ \/ _  / ";
        echo "/___//_//_/\_,_/  ";
        echo "                  ";
    else 
     
        installAnsible
        sleep 1
        installDocker
        sleep 1
        installGo
        sleep 1
        installJq
        sleep 1
        installSponge
        sleep 1
        installNodeJs
        sleep 1
        installNpm
        sleep 5
	
        sudo usermod -aG docker $USER
        echo -e "Installation Successfull !!"
        nextStep
        echo "   ____         __";
        echo "  / __/___  ___/ /";
        echo " / _/ / _ \/ _  / ";
        echo "/___//_//_/\_,_/  ";
        echo "                  ";
    fi
}

installDependencies $1