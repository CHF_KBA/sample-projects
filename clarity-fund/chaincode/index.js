/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const FundingCampaignContract = require('./lib/funding-campaign-contract');

module.exports.FundingCampaignContract = FundingCampaignContract;
module.exports.contracts = [ FundingCampaignContract ];
