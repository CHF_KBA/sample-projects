/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { FundingProposalContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logging = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('FundingProposalContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new FundingProposalContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"funding proposal 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"funding proposal 1002 value"}'));
    });

    describe('#fundingProposalExists', () => {

        it('should return true for a funding proposal', async () => {
            await contract.fundingProposalExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a funding proposal that does not exist', async () => {
            await contract.fundingProposalExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createFundingProposal', () => {

        it('should create a funding proposal', async () => {
            await contract.createFundingProposal(ctx, '1003', 'funding proposal 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"funding proposal 1003 value"}'));
        });

        it('should throw an error for a funding proposal that already exists', async () => {
            await contract.createFundingProposal(ctx, '1001', 'myvalue').should.be.rejectedWith(/The funding proposal 1001 already exists/);
        });

    });

    describe('#readFundingProposal', () => {

        it('should return a funding proposal', async () => {
            await contract.readFundingProposal(ctx, '1001').should.eventually.deep.equal({ value: 'funding proposal 1001 value' });
        });

        it('should throw an error for a funding proposal that does not exist', async () => {
            await contract.readFundingProposal(ctx, '1003').should.be.rejectedWith(/The funding proposal 1003 does not exist/);
        });

    });

    describe('#updateFundingProposal', () => {

        it('should update a funding proposal', async () => {
            await contract.updateFundingProposal(ctx, '1001', 'funding proposal 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"funding proposal 1001 new value"}'));
        });

        it('should throw an error for a funding proposal that does not exist', async () => {
            await contract.updateFundingProposal(ctx, '1003', 'funding proposal 1003 new value').should.be.rejectedWith(/The funding proposal 1003 does not exist/);
        });

    });

    describe('#deleteFundingProposal', () => {

        it('should delete a funding proposal', async () => {
            await contract.deleteFundingProposal(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a funding proposal that does not exist', async () => {
            await contract.deleteFundingProposal(ctx, '1003').should.be.rejectedWith(/The funding proposal 1003 does not exist/);
        });

    });

});