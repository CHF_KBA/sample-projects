/*
* Use this file for functional testing of your smart contract.
* Fill out the arguments and return values for a function and
* use the CodeLens links above the transaction blocks to
* invoke/submit transactions.
* All transactions defined in your smart contract are used here
* to generate tests, including those functions that would
* normally only be used on instantiate and upgrade operations.
* This basic test file can also be used as the basis for building
* further functional tests to run as part of a continuous
* integration pipeline, or for debugging locally deployed smart
* contracts by invoking/submitting individual transactions.
*/
/*
* Generating this test file will also trigger an npm install
* in the smart contract project directory. This installs any
* package dependencies, including fabric-network, which are
* required for this test file to be run locally.
*/

'use strict';

const assert = require('assert');
const fabricNetwork = require('fabric-network');
const SmartContractUtil = require('./js-smart-contract-util');
const os = require('os');
const path = require('path');

describe('FundingProposalContract-crowdFund@0.0.6' , () => {

    const homedir = os.homedir();
    const walletPath = path.join(homedir, 'project', 'network', 'wallets', 'Validator');
    const gateway = new fabricNetwork.Gateway();
    const wallet = new fabricNetwork.FileSystemWallet(walletPath);
    const identityName = 'validatorAdmin';
    let connectionProfile;

    before(async () => {
        connectionProfile = await SmartContractUtil.getConnectionProfile();
    });

    beforeEach(async () => {

        const discoveryAsLocalhost = SmartContractUtil.hasLocalhostURLs(connectionProfile);
        const discoveryEnabled = true;

        const options = {
            wallet: wallet,
            identity: identityName,
            discovery: {
                asLocalhost: discoveryAsLocalhost,
                enabled: discoveryEnabled
            }
        };

        await gateway.connect(connectionProfile, options);
    });

    afterEach(async () => {
        gateway.disconnect();
    });

    describe('fundingProposalExists', () =>{
        it('should check fundingProposa Exists', async () => {
            // TODO: populate transaction parameters
            const arg0 = 'test001';
            const args = [ arg0];

            const response = await SmartContractUtil.submitTransaction('FundingProposalContract', 'fundingProposalExists', args, gateway); // Returns buffer of transaction return value
            // TODO: Update with return value of transaction
            assert.strictEqual(JSON.parse(response.toString()), false);
        }).timeout(10000);
    });

    describe('createFundingProposal', () =>{
        it('should create FundingProposal', async () => {
            // TODO: populate transaction parameters
            const arg0 = 'test001';
            const arg1 = 'Sahadevan';
            const arg2 = 'moolamkuzhiyil';
            const arg3 = '02255';
            const arg4 = 'start ditective agency';
            const arg5 = '200000';
            const args = [ arg0, arg1, arg2, arg3, arg4, arg5];

            const response = await SmartContractUtil.submitTransaction('FundingProposalContract', 'createFundingProposal', args, gateway); 
            const result = await SmartContractUtil.submitTransaction('FundingProposalContract', 'fundingProposalExists', ["test001"], gateway); 
            assert.strictEqual(JSON.parse(result.toString()), true);
        }).timeout(10000);
    });

    describe('readFundingProposal', () =>{
        it('should  read a FundingProposal', async () => {
            const arg0 = 'test001';
            const args = [ arg0];
            const response = await SmartContractUtil.submitTransaction('FundingProposalContract', 'readFundingProposal', args, gateway);
            assert.strictEqual(JSON.parse(response.toString()).username,'Sahadevan');
        }).timeout(10000);
    });

    describe('aceptFundingProposal', () =>{
        it('should  acept a FundingProposal', async () => {
            const arg0 = 'test001';
            const arg1 = 'accept';
            const args = [ arg0, arg1];

            const response = await SmartContractUtil.submitTransaction('FundingProposalContract', 'aceptFundingProposal', args, gateway); // Returns buffer of transaction return value
            const result = await SmartContractUtil.submitTransaction('FundingProposalContract', 'readFundingProposal', ["test001"], gateway);
            assert.strictEqual(JSON.parse(result.toString()).status,'accept' );
        }).timeout(10000);
    });

    describe('deleteFundingProposal', () =>{
        it('should  delete a FundingProposal transaction', async () => {
            const arg0 = 'test001';
            const args = [ arg0];

            await SmartContractUtil.submitTransaction('FundingProposalContract', 'deleteFundingProposal', args, gateway); // Returns buffer of transaction return value
            const response = await SmartContractUtil.submitTransaction('FundingProposalContract', 'fundingProposalExists', ["test001"], gateway);
            assert.strictEqual(JSON.parse(response.toString()), false)
        }).timeout(10000);
    });

});
