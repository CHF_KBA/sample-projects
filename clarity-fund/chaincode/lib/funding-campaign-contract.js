/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const shim = require('fabric-shim');
let logger = shim.newLogger("Chaincode --> ");

class FundingCampaignContract extends Contract {

    async IDExists(ctx, Id) {
        try {
            const buffer = await ctx.stub.getState(Id);
            return (!!buffer && buffer.length > 0);
        } catch (error) {
            logger.error(error.message);
            throw new Error(error.message)
        }
    }


    async createFundingCampaign(ctx, Id, username, address, contactNo, requirement, amountRequired) {
        try {
            let CID = new shim.ClientIdentity(ctx.stub);
            let mspId = CID.getMSPID();
            if (mspId === 'UsersMSP') {
                const exists = await this.IDExists(ctx, Id);
                if (exists) {
                    logger.error(`The funding campaign ${Id} already exists`)
                    throw new Error(`The funding campaign ${Id} already exists`);
                }
                const asset = {
                    username,
                    address,
                    contactNo,
                    requirement,
                    amountRequired,
                    status: 'in Review',
                    amount: 0,
                    amountCollected: 0,
                    depositors: [],
                    type: 'campaign'
                };
                const buffer = Buffer.from(JSON.stringify(asset));
                await ctx.stub.putState(Id, buffer);
                logger.info(`new funding campaign ${Id} created`)
                return true;
            }
            else {
                logger.error(`${mspId} does not have the permission to perform this action`);
                throw new Error(`${mspId} does not have the  permission to perform this action`);
            }

        } catch (error) {
            logger.error(error.message);
            throw new Error(error)
        }

    }

    async readFundingCampaign(ctx, Id) {
        try {
            const exists = await this.IDExists(ctx, Id);
            if (!exists) {
                logger.error(`The funding campaign ${Id} does not exist`)
                throw new Error(`The funding campaign ${Id} does not exist`);
            }
            const buffer = await ctx.stub.getState(Id);
            const asset = JSON.parse(buffer.toString());
            return asset;
        } catch (error) {
            logger.error(error.message);
            throw new Error(error.message)
        }

    }

    async validateFundingCampaign(ctx, Id, newValue) {
        try {
            let CID = new shim.ClientIdentity(ctx.stub);
            const exists = await this.IDExists(ctx, Id);
            if (!exists) {
                logger.error(`The funding campaign ${Id} does not exist`)
                throw new Error(`The funding campaign ${Id} does not exist`);
            }
            let mspId = CID.getMSPID();
            if (mspId === 'ValidatorMSP') {
                const campaign = await ctx.stub.getState(Id);
                const asset = JSON.parse(campaign.toString());
                asset.status = newValue;
                const buffer = Buffer.from(JSON.stringify(asset));
                await ctx.stub.putState(Id, buffer);
                logger.info(`The funding campaign ${Id} status changed to ${newValue}`)
                return true
            }
            else {
                logger.error(`${mspId} does not haver the permission to perform this action`);
                throw new Error(`${mspId} does not have the  permission to perform this action`);
            }
        } catch (error) {
            logger.error(error.message);
            throw new Error(error.message)
        }


    }

    async createWithdrawalRequest(ctx, requestId, username, Id, description, amount, account) {
        try {
            let CID = new shim.ClientIdentity(ctx.stub);
            let mspId = CID.getMSPID();
            if (mspId === 'UsersMSP') {
                const exists = await this.IDExists(ctx, Id);
                if (!exists) {
                    logger.error(`The funding campaign ${Id} does not exist`)
                    throw new Error(`The funding campaign ${Id} does not exist`);
                }
                const campaignBuffer = await ctx.stub.getState(Id);
                const campaign = JSON.parse(campaignBuffer.toString())
                if (!(campaign.username === username)) {
                    logger.error(`User:${campaign.username} only has right to create withdrawal request`)
                    throw new Error(`User:${campaign.username} only has right to create withdrawal request`)
                }
                const depositorsCount = campaign.depositors.length
                const depositors = campaign.depositors
                const asset = {
                    campaignId: Id,
                    description,
                    amount,
                    account,
                    approvals: [],
                    isComplete: false,
                    type: 'request',
                    depositorsCount,
                    depositors
                }
                const buffer = Buffer.from(JSON.stringify(asset));
                await ctx.stub.putState(requestId, buffer);
                logger.info(`new withdrawal request ${requestId} added`)
                return true
            }
            else {
                logger.error(`${mspId} does not haver the permission to perform this action`);
                throw new Error(`${mspId} does not have the  permission to perform this action`);
            }
        } catch (error) {
            logger.error(error.message);
            throw new Error(error.message)
        }

    }

    async depositToCampaign(ctx, Id, username, amount) {
        try {
            let CID = new shim.ClientIdentity(ctx.stub);
            let mspId = CID.getMSPID();
            if (mspId === 'UsersMSP') {
                const exists = await this.IDExists(ctx, Id);
                if (!exists) {
                    logger.error(`The funding campaign ${Id} does not exist`)
                    throw new Error(`The funding campaign ${Id} does not exist`);
                }
                const campaign = await ctx.stub.getState(Id);
                const asset = JSON.parse(campaign.toString());
                if (!asset.depositors.includes(username)) {
                    asset.depositors.push(username);
                }
                if (asset.amountCollected >= asset.amountRequired) {
                    logger.error(`Required Amount Reached`)
                    throw new Error(`Required Amount Reached`);
                }
                asset.amount += parseInt(amount);
                asset.amountCollected += parseInt(amount);
                const buffer = Buffer.from(JSON.stringify(asset));
                await ctx.stub.putState(Id, buffer);
                logger.info(`${username} deposited ${amount} to request ${Id}`)
                return true;
            }
            else {
                logger.error(`${mspId} does not haver the permission to perform this action`);
                throw Error(`${mspId} does not have the  permission to perform this action`);
            }
        } catch (error) {
            logger.error(error.message)
            throw new Error(error.message)
        }

    }

    async approveRequest(ctx, requestId, userId) {
        try {
            let CID = new shim.ClientIdentity(ctx.stub);
            let mspId = CID.getMSPID();
            if (mspId === 'UsersMSP') {
                const exists = await this.IDExists(ctx, requestId);
                if (!exists) {
                    logger.error(`The RequestId: ${requestId} does not exist`)
                    throw new Error(`The RequestId: ${requestId} does not exist`);
                }
                const requestBuffer = await ctx.stub.getState(requestId);
                const request = JSON.parse(requestBuffer.toString());
                if (request.isComplete) {
                    logger.error(`The RequestId: ${requestId} already finalised`)
                    throw new Error(`The RequestId: ${requestId} already finalised`);
                }
                const rightToApprove = request.depositors.includes(userId);
                const alreadyApproved = request.approvals.includes(userId);
                if (alreadyApproved) {
                    logger.error(`This user:${userId} has already approved request ${requestId}`)
                    throw new Error(`This user has already approved this request`)
                }
                if (!rightToApprove) {
                    logger.error(`This user:${userId} does not have permission to aprrove this request`)
                    throw new Error(`This user does not have permission to aprrove this request`);
                }
                request.approvals.push(userId);
                const buffer = Buffer.from(JSON.stringify(request));
                await ctx.stub.putState(requestId, buffer);
                logger.info(`request ${requestId} has been approved by ${userId}`)
                return true
            }
            else {
                logger.error(`${mspId} does not haver the permission to perform this action`);
                throw new Error(`${mspId} does not have the  permission to perform this action`);
            }
        } catch (error) {
            logger.error(error.message)
            throw new Error(error.message)
        }

    }

    async getRequests(ctx, Id) {
        try {
            const exists = await this.IDExists(ctx, Id);
            if (!exists) {
                logger.error(`The funding campaign ${Id} does not exist`)
                throw new Error(`The funding campaign ${Id} does not exist`);
            }
            const queryString = {
                "selector": {
                    "campaignId": Id
                }
            }
            let resultsIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
            let results = await this.getAllResults(resultsIterator, false);
            return JSON.stringify(results);
        } catch (error) {
            logger.error(error.message)
            throw new Error(error.message)
        }

    }

    async finalize(ctx, requestId) {
        try {
            let CID = new shim.ClientIdentity(ctx.stub);
            let mspId = CID.getMSPID();
            if (mspId === 'FundManagerMSP') {
                const exists = await this.IDExists(ctx, requestId);
                if (!exists) {
                    logger.error(`The RequestId: ${requestId} does not exist`)
                    throw new Error(`The RequestId: ${requestId} does not exist`);
                }
                const requestBuffer = await ctx.stub.getState(requestId);
                const request = JSON.parse(requestBuffer.toString());
                const campaignId = request.campaignId
                const campaignBuffer = await ctx.stub.getState(campaignId);
                const campaign = JSON.parse(campaignBuffer.toString());
                const approvalCount = request.approvals.length
                const depositorsCount = request.depositorsCount
                const approvalPercentage = (approvalCount / depositorsCount)
                if (approvalPercentage <= 0.5) {
                    logger.error('This request does not have sufficient approvals')
                    throw new Error('This request does not have sufficient approvals')
                }
                if (campaign.amount < request.amount) {
                    logger.error('insufficient balance')
                    throw new Error('insufficient balance')
                }
                campaign.amount -= request.amount;
                request.isComplete = true
                const buffer1 = Buffer.from(JSON.stringify(request));
                await ctx.stub.putState(requestId, buffer1);
                const buffer2 = Buffer.from(JSON.stringify(campaign));
                await ctx.stub.putState(campaignId, buffer2);
                logger.info(`${requestId} finalized`);
                return true;
            }
            else {
                logger.error(`${mspId} does not haver the permission to perform this action`);
                throw new Error(`${mspId} does not have the  permission to perform this action`);
            }
        } catch (error) {
            logger.error(error.message);
            throw new Error(error.message);
        }

    }
    async getAllCampaigns(ctx, query) {
        try {
            let queryString = {};
            if (query === 'active') {
                queryString = {
                    "selector": {
                        "status": "active"
                    }
                }
            } else {
                queryString = {
                    "selector": {
                        "type": "campaign"
                    }
                };
            }
            let resultsIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString));
            let results = await this.getAllResults(resultsIterator, false);
            return JSON.stringify(results);
        } catch (error) {
            logger.error(error.message);
            throw new Error(error.message);
        }

    }
    async getAllResults(iterator, isHistory) {
        try {
            let allResults = [];
            while (true) {
                let res = await iterator.next();
                if (res.value && res.value.value.toString()) {
                    let jsonRes = {};

                    if (isHistory && isHistory === true) {
                        jsonRes.TxId = res.value.tx_id;
                        jsonRes.Timestamp = res.value.timestamp;
                        jsonRes.IsDelete = res.value.is_delete.toString();
                        try {
                            jsonRes.Value = JSON.parse(res.value.value.toString('utf8'));
                        } catch (err) {
                            logger.error(err);
                            jsonRes.Value = res.value.value.toString('utf8');
                        }
                    } else {
                        jsonRes.Key = res.value.key;
                        try {
                            jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
                        } catch (err) {
                            logger.error(err);
                            jsonRes.Record = res.value.value.toString('utf8');
                        }
                    }
                    allResults.push(jsonRes);
                }
                if (res.done) {
                    await iterator.close();
                    logger.info(allResults);
                    return allResults;
                }
            }
        } catch (error) {
            logger.error(error.message);
            throw new Error(error.message);
        }

    }

    async deleteFundingCampaign(ctx, Id) {
        try {
            const exists = await this.IDExists(ctx, Id);
            if (!exists) {
                logger.error(`The funding campaign ${Id} does not exist`)
                throw new Error(`The funding campaign ${Id} does not exist`);
            }
            await ctx.stub.deleteState(Id);
            logger.info(`Id: ${Id} has been deleted`)
        } catch (error) {
            logger.error(error.message);
            throw new Error(error.message)
        }

    }

}

module.exports = FundingCampaignContract;