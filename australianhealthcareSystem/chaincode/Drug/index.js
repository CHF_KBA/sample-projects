/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const DrugContract = require('./lib/drug-contract');

module.exports.DrugContract = DrugContract;
module.exports.contracts = [ DrugContract ];
