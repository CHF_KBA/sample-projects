/*
 * SPDX-License-Identifier: Apache-2.0
 */

"use strict";

const { Contract } = require("fabric-contract-api");
const shim = require("fabric-shim");

class DrugContract extends Contract {
  //------------Pharmacy--------------------//

  async drugExists(ctx, drugId) {
    const buffer = await ctx.stub.getState(drugId);
    return !!buffer && buffer.length > 0;
  }

  async createDrug(
    ctx,
    drugId,
    Name,
    Strength,
    Type,
    Quantity,
    Company,
    Dom,
    Dex,
    regNo,
    flag
  ) {
    let logger = shim.newLogger("Chaincode --> ");
    let CID = new shim.ClientIdentity(ctx.stub);
    let mspID = CID.getMSPID();
    logger.info("MSPID : " + mspID);

    if (mspID == "PharmacyMSP") {
      const exists = await this.drugExists(ctx, drugId);
      if (exists) {
        throw new Error(`The drug ${drugId} already exists`);
      }
      const asset = {
        Name,
        Strength,
        Type,
        Quantity,
        Company,
        Dom,
        Dex,
        regNo,
        status: "in pharmacy",
        flag,
        GpRegNo: "not assigned",
        PatientId: "not assigned",
      };
      const buffer = Buffer.from(JSON.stringify(asset));
      await ctx.stub.putState(drugId, buffer);
    } else {
      logger.info(
        "Users under the following MSP : " +
          mspID +
          "cannot perform this action"
      );
      return (
        "Users under the following MSP : " +
        mspID +
        "cannot perform this action"
      );
    }
  }

  async readDrug(ctx, drugId) {
    const exists = await this.drugExists(ctx, drugId);
    if (!exists) {
      throw new Error(`The drug ${drugId} does not exist`);
    }
    const buffer = await ctx.stub.getState(drugId);
    const asset = JSON.parse(buffer.toString());
    return asset;
  }

  async deleteDrug(ctx, drugId) {
    let logger = shim.newLogger("Chaincode --> ");
    let CID = new shim.ClientIdentity(ctx.stub);
    let mspID = CID.getMSPID();
    logger.info("MSPID : " + mspID);

    if (mspID == "PharmacyMSP") {
      const exists = await this.drugExists(ctx, drugId);
      if (!exists) {
        throw new Error(`The drug ${drugId} does not exist`);
      }
      await ctx.stub.deleteState(drugId);
    } else {
      logger.info(
        "Users under the following MSP : " +
          mspID +
          "cannot perform this action"
      );
      return (
        "Users under the following MSP : " +
        mspID +
        "cannot perform this action"
      );
    }
  }

  // =================== END Pharmacy==================================//

  // =================== GP==================================//

  async drugExists(ctx, drugId) {
    const buffer = await ctx.stub.getState(drugId);
    return !!buffer && buffer.length > 0;
  }

  async raiseOrder(
    ctx,
    ordNum,
    Name,
    Strength,
    Type,
    Quantity,
    Company,
    Dom,
    Dex,
    regNo,
    GpRegNo,
    PatientId
  ) {
    let logger = shim.newLogger("Chaincode --> ");
    let CID = new shim.ClientIdentity(ctx.stub);
    let mspID = CID.getMSPID();
    logger.info("MSPID : " + mspID);

    if (mspID == "GpMSP") {
      const exists = await this.ordExists(ctx, ordNum);
      if (exists) {
        throw new Error(
          `An unmet order with the same number:${ordNum} already exists`
        );
      }

      const asset = {
        Name,
        Strength,
        Type,
        Quantity,
        Company,
        Dom,
        Dex,
        regNo,
        GpRegNo,
        PatientId,
      };
      const buffer = Buffer.from(JSON.stringify(asset));
      await ctx.stub.putState(ordNum, buffer);
    } else {
      logger.info(
        "Users under the following MSP : " +
          mspID +
          "cannot perform this action"
      );
      return (
        "Users under the following MSP : " +
        mspID +
        "cannot perform this action"
      );
    }
  }

  async readDrug(ctx, drugId) {
    const exists = await this.drugExists(ctx, drugId);
    if (!exists) {
      throw new Error(`The drug ${drugId} does not exist`);
    }
    const buffer = await ctx.stub.getState(drugId);
    const asset = JSON.parse(buffer.toString());
    return asset;
  }

  async deleteOrd(ctx, ordNum) {
    let logger = shim.newLogger("Chaincode --> ");
    let CID = new shim.ClientIdentity(ctx.stub);
    let mspID = CID.getMSPID();
    logger.info("MSPID : " + mspID);

    if (mspID == "GpMSP") {
      const exists = await this.ordExists(ctx, ordNum);
      if (!exists) {
        throw new Error(
          `Unable to delete since an order with: ${ordNum} does not exists`
        );
      }

      return await ctx.stub.deleteState(ordNum);
    } else {
      logger.info(
        "Users under the following MSP : " +
          mspID +
          "cannot perform this action"
      );
      return (
        "Users under the following MSP : " +
        mspID +
        "cannot perform this action"
      );
    }
  }

  async matchOrder(ctx, ordNum, drugId) {
    let logger = shim.newLogger("Chaincode --> ");
    let CID = new shim.ClientIdentity(ctx.stub);
    let mspID = CID.getMSPID();
    logger.info("MSPID : " + mspID);

    if (mspID == "GpMSP") {
      const drugExists = await this.drugExists(ctx, drugId);
      if (!drugExists) {
        throw new Error(
          `A Drug with drug id number: ${drugId} does not exists`
        );
      }

      const ordExists = await this.ordExists(ctx, ordNum);
      if (!ordExists) {
        throw new Error(`Such an order with ${ordNum} does not exists`);
      }

      const drugBuffer = await ctx.stub.getState(drugId);
      const drugDetail = JSON.parse(drugBuffer.toString());

      const orderBuffer = await ctx.stub.getState(ordNum);
      const ordDetail = JSON.parse(orderBuffer.toString());

      if (
        ordDetail.Name === drugDetail.Name &&
        ordDetail.Strength === drugDetail.Strength &&
        ordDetail.Type === drugDetail.Type
      ) {
        drugDetail.GpRegNo = ordDetail.GpRegNo;
        drugDetail.PatientId = ordDetail.PatientId;
        drugDetail.status = "Assigned to Patient";
        drugDetail.flag = 1;

        const newBuffer = Buffer.from(JSON.stringify(drugDetail));
        await ctx.stub.putState(drugId, newBuffer);

        return await this.deleteOrd(ctx, ordNum);
      }
    } else {
      logger.info(
        "Users under the following MSP : " +
          mspID +
          "cannot perform this action"
      );
      return (
        "Users under the following MSP : " +
        mspID +
        "cannot perform this action"
      );
    }
  }

  // =================== END Gp==================================//

  // =================== Selling Drug to the patient ==================================//

  async sellDrug(ctx, drugId, GpRegNo, PatientId) {
    let logger = shim.newLogger("Chaincode --> ");
    let CID = new shim.ClientIdentity(ctx.stub);
    let mspID = CID.getMSPID();
    logger.info("MSPID : " + mspID);

    if (mspID == "PharmacyMSP") {
      const exists = await this.drugExists(ctx, drugId);
      if (!exists) {
        throw new Error(`The Drug with drug Id : ${drugId} does not exists`);
      }

      const drugBuffer = await ctx.stub.getState(drugId);
      const drugDetail = JSON.parse(drugBuffer.toString());

      const stat = "Sold to: " + Patient + " with patient Id: " + PatientId;

      drugDetail.status = stat;
      drugDetail.flag = "sold";

      const newbuffer = Buffer.from(JSON.stringify(drugDetail));
      return await ctx.stub.putState(drugId, newbuffer);
    } else {
      logger.info(
        "Users under the following MSP : " +
          mspID +
          "cannot perform this action"
      );
      return (
        "Users under the following MSP : " +
        mspID +
        "cannot perform this action"
      );
    }
  }

  // =================== Selling Drug to the patient ==================================//
}

module.exports = DrugContract;
