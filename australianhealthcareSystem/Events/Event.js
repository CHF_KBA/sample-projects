const {profile} = require('./profile')
const { FileSystemWallet, Gateway } = require('fabric-network');


class eventLstr{
    setRoleAndIdentity(role,identityLabel){
        this.Profile = profile[role.toLowerCase()]
        let  wallet = new FileSystemWallet(this.Profile["Wallet"])
        this.connectionOptions = {
            identity: identityLabel,
            wallet: wallet,
            discovery: { enabled: true, asLocalhost: true }
        } 

        
    }

    initChannelAndChaincode(channelName,contractName){
        //set channel name
        this.channel = channelName
        //set contract name
        this.contractName = contractName
    }
    async blockEventListner(evetListnerName){
        await this.gateway.connect(this.Profile["CCP"],this.connectionOptions)
        let channel = await this.gateway.getNetwork(this.channel)
        await this.channel.addBlockListener(evetListnerName,(err,block)=>{
            if(err) {
                console.log("An Error Occured : "+ err)
            }
            console.log(block)
        })
    }
}

module.exports = {
    eventLstr
}