let profile = {
    pharmacy: {
        "Wallet":"/home/faizin/AustraliaHealthCare/Network/wallets/Pharmacy",
        "CCP": "/home/faizin/AustraliaHealthCare/Network/gateways/Pharmacy/Pharmacy gateway.json"
    },
    gp : {
        "Wallet":"/home/faizin/AustraliaHealthCare/Network/wallets/Gp",
        "CCP": "/home/faizin/AustraliaHealthCare/Network/gateways/Gp/Gp gateway.json"
    },
    patient:{
        "Wallet":"/home/faizin/AustraliaHealthCare/Network/wallets/Patient",
        "CCP": "/home/faizin/AustraliaHealthCare/Network/gateways/Patient/Patient gateway.json"
    }


}

module.exports = {
    profile
}
