const {eventLstr} = require('./Event')

let PharmacyEventLstr = new eventLstr();
PharmacyEventLstr.setRoleAndIdentity("pharmacy", "admin")
PharmacyEventLstr.initChannelAndChaincode("healthchannel", "Drug")

PharmacyEventLstr.blockEventListner("BlockListner")