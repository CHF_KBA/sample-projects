const {profile} = require('./profile')
const { FileSystemWallet, Gateway } = require('fabric-network');

// sets the location of the wallet
// loads the connectionProfile.a yaml file

// creates an object of the gateway class


class clientApplication{


    setRoleAndIdentity(role,identityLabel){
        this.Profile = profile[role.toLowerCase()]
        let  wallet = new FileSystemWallet(this.Profile["Wallet"])
        this.connectionOptions = {
            identity: identityLabel,
            wallet: wallet,
            discovery: { enabled: true, asLocalhost: true }
        } 

        
    } 

    initChannelAndChaincode(channelName,contractName){
        //set channel name
        this.channel = channelName
        //set contract name
        this.contractName = contractName
    }

    async generatedAndSubmitTxn(txnName,...args){
        let gateway = new Gateway()
        try{
        await gateway.connect(this.Profile["CCP"],this.connectionOptions);
        let channel = await gateway.getNetwork(this.channel);
        let contract = await channel.getContract(this.contractName)
        let result = await contract.submitTransaction(txnName,...args);
        return result
        } catch (error) {

            console.log(`Error processing transaction. ${error}`);
            console.log(error.stack);
    
        } finally {
    
            console.log('Disconnect from Fabric gateway.');
            gateway.disconnect();
    
        }
    }
}

module.exports = {
    clientApplication
}
