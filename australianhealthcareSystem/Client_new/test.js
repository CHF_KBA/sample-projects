const { clientApplication } = require("./client");

let PharmacyClient = new clientApplication();
PharmacyClient.setRoleAndIdentity("Pharmacy","admin")
PharmacyClient.initChannelAndChaincode("healthchannel", "Drug");
// PharmacyClient.generatedAndSubmitTxn(
//     "createDrug",
//     "B213",
//     "Rizact",
//     "40 mg",
//     "rx",
//     "5",
//     "Cipla",
//     "20-09-2020",
//     "20-09-2022",
//     "ABC123",
//     "flag"
//   )
//   .then(message => {
//     console.log(message);
//   });

PharmacyClient.generatedAndSubmitTxn("readDrug", "B213").then(message => {
  console.log(message.toString());
});

// let GpClient = new clientApplication();
// GpClient.setRoleAndIdentity("Gp","admin")
// GpClient.initChannelAndChaincode("healthchannel", "Drug");
// GpClient.generatedAndSubmitTxn(
// "raiseOrder",
// "A213",
// "Rizact",
// "40 mg",
// "rx",
// "5",
// "Cipla",
// "20-09-2020",
// "20-09-2022",
// "ABC123",
// "gp123",
// "p123",
// )
// .then(message => {
//       console.log(message);
//     });