Introduction

The covid-19 pandemic has brought to light the fragility of the global medicine supply chain.
Medicine shortages both locally and globally have exposed the need for a new system of management, one that ensures patients have access to medicines when they need them. The manual task of issuing scripts for a patient to receive medicines at a pharmacy is outdated and prone to error, fraudulent claims, secondary products and confusion. The paper script method of issuing medicines could be reimagined by blockchain technology, ensuring equity, fair distribution and authenticity of medicines in a trustless way. Developing countries are exposed to more complicated problems arising from black market medicines, pseudo pharmacies and recently, the emerging online pharmacy industry. A blockchain solution is applicable to the regulation and distribution of medicines, locally and globally.

Problem Statements

● A general practitioner (GP) wants to ensure when they provide a script for medicines that the patient who is requesting them is genuine
● A patient who requests a script from their GP for medicines can acquire said medicines at their local pharmacy
● A pharmacy wants to ensure the validity of a paper script when a patient requests medicines at their premises
● A pharmacy would like to ensure the authenticity of generic and branded medicines for their own quality of service to customers
● A patient would like to ensure the authenticity of generic and branded medicines for their own safety
● A GP would like to ensure their patients are able to access medicines that they prescribe to them.
● A GP issues an owing script to a patient in good faith, but the GP is unaware if the patient has for certain finished their previous script and require the new one at that time
● Schedule 3 medicine scripts such as opioids are increasingly fraudulent claims by patients who may have become addicted to the substances
● A GP could be issuing scripts to a patient who is actually selling them in a black market situation, putting people at risk

Participants and vectors:


Can a Blockchain solve the problem? Applying the Hype Test.

1. Do multiple parties need to interact with the data?
Yes, the GP and pharmacies would need to interact with the data. The patient could potentially access data on whether a particular drug is available at a particular pharmacy.

2. Do you require high performance transactions?
No, the system would not require millisecond measured transactions. Transactions would occur at the GP. The GP would access the patient data using an identifier to unlock off chain data. The GP would make alterations of the data based on the patient requirements. If a prescription is issued this would be added to the patient data. Second transaction point is at the pharmacy. After a visit to the GP and prescription medicine is advised, the patient would visit their local pharmacy and provide the pharmacist with their identifier. The pharmacist would then access the off chain patient data. The pharmacist would access the GP recommended medicines and provide those drugs to the patient.

3. Do you need to store large amounts of data?
No, patient data would be stored off chain as the data is sensitive and private. An identifier would be used to access the off chain data for the patient.

4. Is there mistrust between the various parties interacting with the data?
Yes, there is a mistrust issue between the GP issuing a script for a drug and the patient providing a pharmacy with an authentic script. There are mistrust issues between the GP and the patient providing genuine claims for owing scripts. Currently the “good faith” issuance of owing scripts is based on a patient telling the truth.

5. Are there third parties that everyone can trust?
No, currently there is no national database run by the Australian government that provides third party assurance.

6. Do multiple parties need to update or change stored data?
Yes, a GP would need to update patient data by adding the details of new script requests. A pharmacy would need to update the submission of a script as well as the issuance of a drug per script.

7. Will all parties that update data be able to agree on the rules of who/how/when data can be updated?
Yes, doctors and pharmacies would work seamlessly as they both provide a part of the same service to their patients and customers.

8. Will there be enough network participants to reach a consensus about what is a valid transaction?
Yes, a network of doctors and pharmacies could validate the authenticity of an addition of a script to a patient. If a script has been issued to a patient that script would no longer be eligible to be used in another pharmacy. Previous script / drug issuance would be stored on the patient off chain data. A pharmacist or GP could access data on when a script was issued, when it was collected at a pharmacy, the length of time the prescription should cover, and any discrepancies therein.

9. If permissions are required, can agreement be reached on who/how user rights will be granted?
Yes, permissions could be allocated within the organisation. In the case of a doctor's surgery and a pharmacy, only licenced GP’s and licensed pharmacists would be given permission to access, add or edit the data.

Can a Blockchain solve the problem? Applying the PPCO Tool.
Pluses:
A blockchain solution would create certainty between key stakeholders in a trustless manner. The GP would be assured of the patients validity when requesting a script for a drug. The patient would be assured that a requested drug is available at a particular pharmacy. A pharmacy would be assured that a patient script is genuine. Key stakeholders would be provided with assurance of the genuinity and validity of each stage of the chain of supply.

Potentials:
Drug misuse would be minimised. Harm by use of illegally acquired schedule 3 drugs would be minimised. Paper script prone errors would be eradicated. Good faith would not be an element of owing scripts. Misuse of the “owing script “ loophole for misuse of drugs would be eradicated. Fraudulent claims for prescriptions would be minimised.

Concerns:
How might patient data be stored on a blockchain? How might the system provide certainty that patient data could not be compromised? How might the system provide certainty that duplicate information would not arise?

Overcoming Concerns:
Patient data would need to be stored in off chain storage. A patient identifier would be required to
access off chain data storage.

The Blockchain Solution

A GP prescribed paper script is an outdated system of drug issuance that can be compromised by fraudulent claims by patients. The system is prone to error and in some cases, such as owing scripts, based on good faith. A trustless permissioned blockchain with off chain storage like Hyperledger Fabric would provide the solution to these problems.

Participants of the blockchain would be hierarchical, GP’s and pharmacies would allocate permissions based on employment status. A general pharmacy clerk would not be able to make changes to the blockchain, whereas a licenced pharmacist issuing the drugs would. A licenced GP would access and edit the data but the secretary at the doctors surgery would not have permission. A patient/customer could access information on the availability of the drugs at any particular pharmacy that has been prescribed by the GP.

A paper script would no longer be necessary as the prescribed drugs by the GP would be added to the patient data on the off chain storage by accessing the patient data via an identifier. The patient would only need to provide the identifier at the pharmacy to access their prescription. A website user interface could be created to provide patients access to drug availability at a particular pharmacy.

WorkFlow:

    • Create an item for sale (drug)- invoked by Pharmacy.
    • Create a script – invoked by GP.
    • Match the GP’s script with appropriate drug listed for sale by the Pharmacy – invoked by pharmacy
    • Sell (assign) the drug to the patient after verifying his id – invoked by Pharmacy


Digital Asset:

Digital Asset =>  Drug
“Drug” :{
		 drugId,
    Name,
    Strength,
    Type,
    Quantity,
    Company,
    Dom,
    Dex,
    regNo,
    flag
}

Task 1: Implement CRUD Operations

    1. DrugExists: This function will check whether the drug is already created or not.
    2. addDrug: This function will add new drug for sale.
    3. readDrug: Query information of Drug asset.
    4. updateDrug: Alter the values of a Drug asset.
    5. deleteDrug: For removing ITEM asset when a particular drug is banned or prohibited.
    6. raiseOrder: Create a script for a patient
    7. matchOrder: Match the drug available at the pharmacy before prescribing it to the patient.
    8. sellDrug: Sell the drug to the patient after verifying the gp ID and patient ID.


Task 3: Implement a method to begin the sale of the drug by setting up the status an “in stock” and state as “for sale”. A GP can prescribe a drug that is “for sale” state. While setting a drug for sale, the pharmacy has to set up the selling price of the drug and quantity sold as 0 for tracking the quantity sold. 

Define the below value
    • status - “in stock”
    • state – “for sale”

Task 4: Prescribe a drug

GP can prescribe a drug. This methid can be only invoked by a registered practitioner. Before implementing the business logic, we have to check the state of the ITEM asset and make sure that the ITEM is in stock and is for sale. The business logic always stores the GP’s name, patient’s name and the quatity sold. Also, decrement the available quantity to track the quantity in stock.

Task 5: Close the sale of a specific drug.

At times, we need to stop the sale of a specific drug as advised by the health ministry. This function can be invoked by the pharmacy.

Do the following changes to the asset :
    • status -> “not available”
    • state -> “not for sale”


The Pharmacy should be able to create and delete an Item:-
    •     drugExists() - check whether the drugalready exists
    •     addDrug () - create a new drug object
    •     readDrug () - read drug details
    •     deleteDrug () - remove drug details
    • sellDrug() - sell a drug to the patient

The GP should be able to create orders in the script and match the drugs added by pharmacy with the order. Following are the list of functions for the GP:-
    •     ordExists() - check if the order exists
    •     raiseOrder() - create a new order
    •     readOrder() - read order details
    •     matchOrder() - match a drug with the order
    •     deleteOrd() - delete an order
Each order will be uniquely identified using an order number and patient Id.



