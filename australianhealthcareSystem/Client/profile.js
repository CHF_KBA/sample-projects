
let profile = {
    pharmacy: {
        "Wallet":"../wallets/Pharmacy",
        "CCP": "<path to network folder>/gateways/Pharmacy/Pharmacy gateway.json"
    },
    gp : {
        "Wallet":"../wallets/Gp",
        "CCP": "<path to network folder>/gateways/Gp/Gp gateway.json"
    },
    patient:{
        "Wallet":"../wallets/Patient",
        "CCP": "<path to network folder>/gateways/Patient/Patient gateway.json"
    }


}

module.exports = {
    profile
}