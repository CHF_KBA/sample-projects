**CHFVB604_BankGuarantee**

Web App link : https://bank-guarantee-798bc.web.app/

_Installation_

- Clone the repository
    - git clone  https://gitlab.com/sibin1000/chfvb604_bankguarantee.git


- Setup network
    - cd bank_guarantee_network
    - ansible-playbook playbook.yml


- Run client app
    - cd client
    - node index.js
    

- Open web app in browser
    - https://bank-guarantee-798bc.web.app/
    - Please note that the webapp will connect to localhost:3000 of the client app. So make sure your client app is running before using the web app

