
let profile = {

    bankone: {
        "Wallet": "/Users/sibinjoseph/kba/Project/bank_guarantee_network/wallets/BankOne",
        "CCP": "/Users/sibinjoseph/kba/Project/bank_guarantee_network/gateways/BankOne/BankOne gateway.json"
    },
    banktwo: {
        "Wallet": "/Users/sibinjoseph/kba/Project/bank_guarantee_network/wallets/BankTwo",
        "CCP": "/Users/sibinjoseph/kba/Project/bank_guarantee_network/gateways/BankTwo/BankTwo gateway.json"
    },
    rbi: {
        "Wallet": "/Users/sibinjoseph/kba/Project/bank_guarantee_network/wallets/RBI",
        "CCP": "/Users/sibinjoseph/kba/Project/bank_guarantee_network/gateways/RBI/RBI gateway.json"
    }

}

module.exports = {
    profile
}