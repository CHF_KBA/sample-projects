/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const GuaranteeContract = require('./lib/guarantee-contract');

module.exports.GuaranteeContract = GuaranteeContract;
module.exports.contracts = [ GuaranteeContract ];
